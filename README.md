# ![logo](video-promo-400x400.png) **Création Vidéo**

## [**VLC portable**](https://get.videolan.org/vlc/3.0.8/win32/vlc-3.0.8-win32.zip) : Enregistrer le Bureau de son PC

VLC permet de capturer l’écran de l’ordinateur en vidéo. 

Vous pourrez ainsi créer un tutoriel vidéo comme celui que vous êtes en train de visionner, ou encore prouver vos prouesses dans un jeu vidéo.

Pour lancer une capture vidéo, lancez la commande Convertir / Enregistrer dans le menu Média, ou appuyez simultanément sur les touches Contrôle et R. 

La boîte de dialogue Ouvrir un média s’affiche. 

Basculez sur l’onglet Périphérique de capture. 

+ Choisissez Bureau dans la liste déroulante Mode de capture (pour capturer l’écran) et 25 dans la zone de texte Débit d’images pour la capture (pour choisir le nombre d’images capturées par seconde) :

![vlc1](images/vlc1.PNG)


+ Cliquez sur Convertir / Enregistrer dans la partie inférieure de la fenêtre. Une nouvelle boîte de dialogue intitulée Convertir s’affiche :

![vlc2](images/vlc2.PNG)

+ Choisissez un format d’enregistrement dans la liste déroulante Profil en fonction du type de fichier que vous voulez obtenir. 
Par exemple Théora + Vorbis (OGG) qui est peu gourmand en ressources système.
Cliquez sur Parcourir et définissez le dossier de sauvegarde et le nom du fichier dans lequel sera sauvegardé la vidéo de capture :


![vlc3](images/vlc3.PNG)


+ Lorsque vous êtes prêt à commencer la capture vidéo, cliquez sur Démarrer. 
La fenêtre de VLC reste ouverte. Vous pouvez la replier pour capturer l’écran de l’ordinateur. 
Lorsque vous voudrez arrêter la capture, cliquez sur l’icône de VLC dans la barre des tâches, puis cliquez sur le bouton Arrêter la lecture dans la partie inférieure gauche de la fenêtre :

![vlc4](images/vlc4.PNG)


# ![clap](images/clap.png) **Projet**
> **Choisir un des thèmes sur lequel on a travaillé dans le module TIC et réaliser une vidéo de cours de moins de 10 minutes.**